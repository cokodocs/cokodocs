const merge = require('lodash/merge')
const user = require('./user')
const doc = require('./doc')
const docTree = require('./docTree')

module.exports = {
  typeDefs: [user.typeDefs, doc.typeDefs, docTree.typeDefs].join(' '),
  resolvers: merge({}, user.resolvers, doc.resolvers, docTree.resolvers),
}
