/* eslint-disable react/destructuring-assignment */
/* stylelint-disable no-descending-specificity, declaration-no-important */

import React, { useState } from 'react'
import { useHistory } from 'react-router-dom'
import styled from 'styled-components'
import {
  FolderAddFilled,
  FileAddFilled,
  DeleteFilled,
  EditFilled,
  CloseCircleFilled,
} from '@ant-design/icons'
import Button from '../../common/Button'

const RowContainer = styled.div`
  color: ${props => (props.isActive ? 'black' : 'inherit')};
  display: flex;
  flex-direction: column;
  font-weight: ${props => (props.isActive ? '600' : 'normal')};
  margin-bottom: 5px;
  margin-top: 10px;
`

const TitleToolsContainer = styled.div`
  display: flex;
  flex-direction: row;
`

const ToolsContainer = styled.div`
  display: flex;
  flex-direction: row;
  margin-left: auto;
`

const StyledFolderFileBtn = styled(Button)`
  background-color: transparent;
  border: none;
  margin-left: 10px;
  outline: 0 !important;
  padding: 0;
  text-decoration: none;
  width: fit-content;

  svg {
    fill: #6db6d6;

    &:active,
    &:focus,
    &:hover {
      fill: #49a4cc;
    }
  }
`

const StyledInput = styled.input`
  background-color: #f6edf6;
  border: 2px solid #a34ba1;
  margin-right: 5px;

  :focus {
    outline: none;
  }
`

const StyledApplyButton = styled(Button)`
  background-color: #a34ba1;
  color: #fff;

  &:hover {
    background-color: #a34ba1 !important;
    color: #fff !important;
  }
`

const IconTitleContainer = styled.div`
  display: flex;
  flex-direction: row;

  span {
    margin-bottom: 10px;
    margin-right: 5px;

    svg {
      fill: #a34ba1;
    }
  }
`

let lock = false

const RowRender = row => {
  const {
    id,
    title,
    renameResource,
    addResource,
    isFolder,
    identifier,
    confirmDelete,
    showTools,
    isActive,
  } = row

  const history = useHistory()
  const [updatedName, setUpdateName] = useState(title)
  const [isRename, setRename] = useState(false)

  const setActive = () => {
    Array.from(document.getElementsByClassName('rowContainer')).forEach(
      element => {
        /* eslint-disable-next-line no-shadow */
        const id = element.getAttribute('id')

        if (id === identifier) {
          /* eslint-disable-next-line no-param-reassign */
          element.style.color = 'black'
          /* eslint-disable-next-line no-param-reassign */
          element.style.fontWeight = '600'
        } else {
          /* eslint-disable-next-line no-param-reassign */
          element.style.color = 'inherit'
          /* eslint-disable-next-line no-param-reassign */
          element.style.fontWeight = 'normal'
        }
      },
    )
  }

  /* eslint-disable-next-line consistent-return */
  const goToDocument = e => {
    if (!lock) {
      lock = true
      setTimeout(() => {
        lock = false
      }, 1500)

      if (e.target.type === 'text') {
        e.preventDefault()
        return false
      }

      if (!isFolder) {
        history.push(`/${identifier}`, { replace: true })
        setActive()
      }
    }
  }

  return (
    <RowContainer
      className="rowContainer"
      id={identifier}
      isActive={isActive}
      onClick={e => goToDocument(e)}
    >
      <TitleToolsContainer>
        {isRename ? (
          <>
            <StyledInput
              autoFocus
              onChange={e => setUpdateName(e.target.value)}
              onKeyDown={e => {
                if (e.key === 'Enter' || e.keyCode === 13) {
                  renameResource({ variables: { id, title: updatedName } })
                  setRename(false)
                }
              }}
              type="text"
              value={updatedName}
            />
            <StyledApplyButton
              onClick={() => {
                renameResource({ variables: { id, title: updatedName } })
                setRename(false)
              }}
              onMouseDown={e => {
                e.preventDefault()
                renameResource({ variables: { id, title: updatedName } })
                setRename(false)
              }}
            >
              Apply
            </StyledApplyButton>
          </>
        ) : (
          <IconTitleContainer>
            {isFolder && <FolderAddFilled style={{ fontSize: '19px' }} />}
            <span>
              {!row.isRoot && title.length > 18
                ? `${title.substring(0, 18)}...`
                : title}
            </span>
          </IconTitleContainer>
        )}
        {!row.isRoot && showTools && (
          <ToolsContainer>
            {isFolder && (
              <>
                <StyledFolderFileBtn
                  onClick={() =>
                    addResource({ variables: { id, isFolder: true } })
                  }
                  title="Add Folder"
                >
                  <FolderAddFilled style={{ fontSize: '19px' }} />
                </StyledFolderFileBtn>
                <StyledFolderFileBtn
                  onClick={() =>
                    addResource({ variables: { id, isFolder: false } })
                  }
                  title="Add File"
                >
                  <FileAddFilled style={{ fontSize: '19px' }} />
                </StyledFolderFileBtn>
              </>
            )}

            <StyledFolderFileBtn
              onMouseDown={e => {
                e.preventDefault()
                confirmDelete(row)
              }}
              title="Delete"
            >
              <DeleteFilled style={{ fontSize: '19px' }} />
            </StyledFolderFileBtn>

            {isRename ? (
              <StyledFolderFileBtn
                onMouseDown={e => {
                  e.preventDefault()
                  setRename(false)
                }}
                title="Close"
              >
                <CloseCircleFilled style={{ fontSize: '22px' }} />
              </StyledFolderFileBtn>
            ) : (
              <StyledFolderFileBtn
                onMouseDown={e => {
                  e.preventDefault()
                  setRename(true)
                }}
                title="Rename"
              >
                <EditFilled style={{ fontSize: '22px' }} />
              </StyledFolderFileBtn>
            )}
          </ToolsContainer>
        )}
      </TitleToolsContainer>
    </RowContainer>
  )
}

export default RowRender
