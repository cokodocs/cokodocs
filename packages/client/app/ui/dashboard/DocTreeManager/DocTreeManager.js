/* eslint-disable react/prop-types, react/no-unstable-nested-components */
/* stylelint-disable no-descending-specificity, declaration-no-important */

import React, { useEffect, useState } from 'react'
import { Tree } from 'antd'
import styled from 'styled-components'
import { grid } from '@coko/client'
import {
  FolderAddFilled,
  FileAddFilled,
  VerticalAlignBottomOutlined,
} from '@ant-design/icons'
import Button from '../../common/Button'
import RowRender from './RowRender'
import ConfirmDelete from '../../modals/ConfirmDelete'

const DocTreeManagerWrapper = styled.div`
  display: flex;
  flex-direction: row;
  height: 100%;
  width: 100%;

  @keyframes slideLeft {
    0% {
      transform: translateX(0%);
    }

    100% {
      transform: translateX(-100%);
      visibility: hidden;
    }
  }

  @keyframes slideRight {
    0% {
      transform: translateX(0%);
      visibility: visible;
    }

    100% {
      transform: translateX(0%);
    }
  }
`

const ControlsWrappers = styled.div`
  align-items: center;
  background: #f6edf6;
  display: flex;
  flex-direction: column;
  height: 100%;
  padding: ${grid(2)};
  width: 10%;
  z-index: 1;
`

const FilesWrapper = styled.div`
  animation: ${props =>
    props.expand ? 'slideRight 2s forwards' : 'slideLeft 1s forwards'};
  background: white;
  display: flex;
  flex-direction: column;
  height: 100%;
  overflow: auto;
  padding: 8px 8px 8px 0;
  visibility: ${props => (props.defaultState ? 'visible' : 'hidden')};
  width: 90%;

  .ant-tree {
    background: white;
  }

  .ant-tree-treenode-disabled {
    color: black !important;

    span {
      color: black !important;
    }
  }

  ant-tree-title:hover {
    background: #c8e4f0 !important;
  }

  span.ant-tree-node-selected {
    background: #edf6fa !important;
  }

  .ant-tree-switcher {
    margin-top: 10px;
  }

  .ant-tree-draggable-icon {
    cursor: grab;
    margin-top: 10px;
    opacity: 1 !important;

    span svg {
      fill: #6db6d6;
    }
  }
`

const StyledMainButton = styled(Button)`
  background-color: transparent;
  border: none;
  margin-bottom: ${grid(4)};
  outline: 0 !important;
  padding: 0;
  text-decoration: none;
  width: fit-content;

  svg {
    fill: #a34ba1;

    &:active,
    &:focus,
    &:hover {
      fill: #723571;
    }
  }
`

const StyledMainButtonExpand = styled(StyledMainButton)`
  svg {
    transform: ${props =>
      props.expand === 'true' ? 'rotate(90deg)' : 'rotate(-90deg)'};
  }
`

const SharedTree = styled(Tree)`
  margin-left: ${grid(6)};
`

const DocTreeManager = ({
  getDocTreeData,
  addResource,
  renameResource,
  reorderResource,
  deleteResource,
}) => {
  let isFileManagerOpen = true

  if (localStorage.getItem('isFileManagerOpen') !== null) {
    isFileManagerOpen = localStorage.getItem('isFileManagerOpen')
  } else {
    localStorage.setItem('isFileManagerOpen', isFileManagerOpen)
  }

  const [gData, setGData] = useState([])
  const [sharedDocTree, setSharedDocTree] = useState([])
  const [deleteResourceRow, setDeleteResourceRow] = useState(null)

  const [expandFilesArea, setExpandFilesArea] = useState(
    isFileManagerOpen === 'true',
  )

  const [defaultState, setDefaultState] = useState(expandFilesArea)

  // const [expandedKeys] = useState(['0-0', '0-0-0', '0-0-0-0'])

  const findChildNodeByIdentifier = (data, identifier) => {
    /* eslint-disable-next-line no-restricted-syntax */
    for (const node of data) {
      if (node.children) {
        /* eslint-disable-next-line no-restricted-syntax */
        for (const child of node.children) {
          if (child.identifier === identifier) {
            return child
          }
        }

        const found = findChildNodeByIdentifier(node.children, identifier)

        if (found) {
          return found
        }
      }
    }

    return null
  }

  const findParentNode = (data, childKey) => {
    /* eslint-disable-next-line no-restricted-syntax */
    for (const node of data) {
      if (node.children) {
        /* eslint-disable-next-line no-restricted-syntax */
        for (const child of node.children) {
          if (child.key === childKey) {
            return node
          }
        }

        const found = findParentNode(node.children, childKey)

        if (found) {
          return found
        }
      }
    }

    return null
  }

  const onDrop = async info => {
    const dropKey = info.node.key
    const dragKey = info.dragNode.key
    const dropPos = info.node.pos.split('-')
    const dropPosition = info.dropPosition - Number(dropPos[dropPos.length - 1]) // the drop position relative to the drop node, inside 0, top -1, bottom 1

    /* eslint-disable-next-line consistent-return */
    const loop = (data, key, callback) => {
      /* eslint-disable-next-line no-plusplus */
      for (let i = 0; i < data.length; i++) {
        if (data[i].key === key) {
          return callback(data[i], i, data)
        }

        if (data[i].children) {
          loop(data[i].children, key, callback)
        }
      }
    }

    const data = [...gData]

    // Find dragObject
    let dragObj
    loop(data, dragKey, (item, index, arr) => {
      arr.splice(index, 1)
      dragObj = item
    })

    if (!info.dropToGap) {
      // Drop on the content
      loop(data, dropKey, item => {
        /* eslint-disable-next-line no-param-reassign */
        item.children = item.children || []
        // where to insert. New item was inserted to the start of the array in this example, but can be anywhere
        item.children.unshift(dragObj)
      })
    } else {
      let ar = []
      let i
      loop(data, dropKey, (_item, index, arr) => {
        ar = arr
        i = index
      })

      if (dropPosition === -1) {
        // Drop on the top of the drop node
        ar.splice(i, 0, dragObj)
      } else {
        // Drop on the bottom of the drop node
        ar.splice(i + 1, 0, dragObj)
      }
    }

    setGData(data)

    const newParentNode = findParentNode(data, dragKey)
    if (!newParentNode) return

    const newPosition = newParentNode.children.findIndex(
      child => child.key === dragKey,
    )

    await reorderResource({
      variables: { id: dragKey, newParentId: newParentNode.key, newPosition },
    })
  }

  useEffect(async () => {
    const { data } = await getDocTreeData()
    const allData = JSON.parse(data.getDocTree)

    if (allData.length > 0) {
      allData[0].disabled = true
      allData[0].isRoot = true
      allData[0].title = 'My Folders and Files'
    }

    setGData([...allData])

    setSharedDocTree([...data.getSharedDocTree])
  }, [])

  const addResourceFn = async variables => {
    await addResource(variables)
    const { data } = await getDocTreeData()
    const allData = JSON.parse(data.getDocTree)
    allData[0].disabled = true
    allData[0].isRoot = true
    allData[0].title = 'My Folders and Files'
    setGData([...allData])
  }

  const renameResourceFn = async variables => {
    await renameResource(variables)
    const { data } = await getDocTreeData()
    const allData = JSON.parse(data.getDocTree)
    allData[0].disabled = true
    allData[0].isRoot = true
    allData[0].title = 'My Folders and Files'
    setGData([...allData])
  }

  const deleteResourceFn = async variables => {
    await deleteResource(variables)
    const { data } = await getDocTreeData()
    const allData = JSON.parse(data.getDocTree)
    allData[0].disabled = true
    allData[0].isRoot = true
    allData[0].title = 'My Folders and Files'
    setGData([...allData])
  }

  const confirmDelete = row => {
    setDeleteResourceRow(row)
  }

  const parts = window.location.href.split('/')
  const currentIdentifier = parts[parts.length - 1]

  const getActiveDocForDeletion = findChildNodeByIdentifier(
    deleteResourceRow ? [deleteResourceRow] : [],
    currentIdentifier,
  )

  return (
    <DocTreeManagerWrapper>
      <ControlsWrappers>
        <StyledMainButton
          onClick={() =>
            addResourceFn({ variables: { id: null, isFolder: true } })
          }
          title="Add Folder"
        >
          <FolderAddFilled style={{ fontSize: '32px' }} />
        </StyledMainButton>
        <StyledMainButton
          onClick={() =>
            addResourceFn({ variables: { id: null, isFolder: false } })
          }
          title="Add File"
        >
          <FileAddFilled style={{ fontSize: '32px' }} />
        </StyledMainButton>
        <StyledMainButtonExpand
          expand={expandFilesArea.toString()}
          onClick={() => {
            setDefaultState(true)
            localStorage.setItem('isFileManagerOpen', !expandFilesArea)
            setExpandFilesArea(!expandFilesArea)
          }}
          title="Show / Hide Filemanager"
        >
          <VerticalAlignBottomOutlined style={{ fontSize: '32px' }} />
        </StyledMainButtonExpand>
      </ControlsWrappers>
      <FilesWrapper defaultState={defaultState} expand={expandFilesArea}>
        <Tree
          allowDrop={node => {
            if (
              (node.dropPosition <= 0 && node.dropNode.isRoot) ||
              (node.dropPosition === 0 && !node.dropNode.isFolder)
            ) {
              return false
            }

            return true
          }}
          blockNode
          className="draggable-tree"
          defaultExpandAll
          draggable
          key="myDocs"
          onDrop={onDrop}
          titleRender={rowProps => {
            return (
              <RowRender
                {...rowProps}
                addResource={addResourceFn}
                confirmDelete={confirmDelete}
                renameResource={renameResourceFn}
                showTools
              />
            )
          }}
          treeData={gData}
          // defaultExpandedKeys={expandedKeys}
        />

        <SharedTree
          blockNode
          key="sharedDocTree"
          titleRender={rowProps => {
            return <RowRender {...rowProps} />
          }}
          treeData={sharedDocTree}
        />
      </FilesWrapper>
      <ConfirmDelete
        deleteResourceFn={deleteResourceFn}
        deleteResourceRow={
          deleteResourceRow?.isFolder
            ? getActiveDocForDeletion || deleteResourceRow
            : deleteResourceRow
        }
        setDeleteResourceRow={setDeleteResourceRow}
      />
    </DocTreeManagerWrapper>
  )
}

export default DocTreeManager
