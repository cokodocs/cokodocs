/* eslint-disable react/prop-types */

import React, { useMemo } from 'react'
import { useParams } from 'react-router-dom'
import PmEditor from '../wax/PmEditor'
import DocTreeManager from './DocTreeManager/DocTreeManager'

const Dashboard = ({
  addResource,
  renameResource,
  deleteResource,
  reorderResource,
  getDocTreeData,
  showFilemanager,
}) => {
  const { docIdentifier } = useParams()

  localStorage.removeItem('nextDocument')

  const createDocTreeManager = useMemo(() => {
    return (
      <DocTreeManager
        addResource={addResource}
        deleteResource={deleteResource}
        getDocTreeData={getDocTreeData}
        renameResource={renameResource}
        reorderResource={reorderResource}
      />
    )
  }, [])

  return (
    <PmEditor
      docIdentifier={docIdentifier}
      docTreeManager={createDocTreeManager}
      showFilemanager={showFilemanager}
    />
  )
}

Dashboard.propTypes = {}

Dashboard.defaultProps = {}

export default Dashboard
