/* stylelint-disable string-quotes, declaration-block-no-redundant-longhand-properties */

import React, { useContext, useEffect, useRef, useState } from 'react'
import styled, { ThemeProvider } from 'styled-components'
import { WaxContext, ComponentPlugin } from 'wax-prosemirror-core'
import { grid, th, override } from '@coko/client'
import { EllipsisOutlined } from '@ant-design/icons'
import { Scrollbars } from 'react-custom-scrollbars-2'
import YjsContext from '../../../yjsProvider'
import theme from '../../../theme'
import commonStyles from './cokoDocsWaxStyles'
import MenuComponent from './MenuComponent'

import 'wax-table-service/dist/index.css'
import 'wax-prosemirror-core/dist/index.css'
import 'wax-prosemirror-services/dist/index.css'

const Wrapper = styled.div`
  background: ${th('colorBackground')};
  font-family: '${th('fontInterface')}';
  font-size: ${th('fontSizeBase')};
  height: calc(100% - ${props => props.menuHeight}px);
  line-height: ${grid(4)};
  width: 100%;

  * {
    box-sizing: border-box;
  }
`

const EditorArea = styled.div`
  background: ${th('colorBackground')};
  height: 100%;
  width: 100%;
`

const WaxBottomRightInfo = styled.div``

const InfoContainer = styled.div`
  bottom: 1px;
  display: flex;
  position: fixed;
  right: 21px;
  z-index: 999;

  span {
    font-size: 14px;
  }

  div div div div {
    color: #525e76;
    height: auto;
    margin: 0;
    padding-bottom: 6px;
    width: 235px;
  }
`

const EditorContainer = styled.div`
  float: left;
  height: 100%;
  padding-top: 20px;
  position: relative;

  .ProseMirror {
    box-shadow: 0 0 8px #ecedf1;
    margin-top: 4rem;
    min-height: 100%;
    padding: ${grid(10)};
    width: unset;
  }

  /* stylelint-disable-next-line order/properties-alphabetical-order */
  ${commonStyles}
`

const MenuWrapper = styled.div`
  background-color: white;
  border-bottom: 1px solid gainsboro;
  border-top: 1px solid gainsboro;
  display: flex;
  flex-direction: row;
  flex-wrap: nowrap;
  font-size: 16px;

  div:last-child {
    margin-left: auto;
  }
`

const ShowMore = styled(EllipsisOutlined)`
  display: none;
  font-size: 40px;
  margin-left: auto;
  right: 10px;

  @media screen and (max-width: 1050px) {
    display: flex;
    position: relative;
    right: 10px;
    top: 0;
  }
`

const CommentsContainer = styled.div`
  display: flex;
  flex-direction: column;
  height: 100%;
  position: relative;
  width: 300px;

  div[data-box] {
    button {
      font-size: 14px;
    }

    span {
      font-size: 11px;
    }
  }
`

const WaxSurfaceScroll = styled.div`
  box-sizing: border-box;
  display: flex;
  height: 100%;
  margin: auto;
  position: relative;
  width: 1350px;

  /* stylelint-disable-next-line order/properties-alphabetical-order */
  ${override('Wax.WaxSurfaceScroll')}
`

const CokoDocsEditorWrapper = styled.div`
  display: flex;
  flex-direction: row;
  height: 100%;
`

const FileManagerWrapper = styled.div`
  display: flex;
  flex-direction: row;
  margin-right: 3px;
  width: 700px;
`

const BottomRightInfo = ComponentPlugin('BottomRightInfo')
const RightArea = ComponentPlugin('rightArea')

const Layout =
  (DocTreeManager, showFilemanager) =>
  /* eslint-disable-next-line react/function-component-definition, react/prop-types */
  ({ editor }) => {
    const {
      pmViews: { main },
    } = useContext(WaxContext)

    const { sharedUsers, yjsCurrentUser } = useContext(YjsContext)

    const ref = useRef(null)
    const [open, toggleMenu] = useState(false)
    const [menuHeight, setMenuHeight] = useState(42)

    useEffect(() => {
      if (ref.current) {
        setMenuHeight(ref.current.clientHeight + 2)
      }
    }, [open])

    useEffect(() => {
      const handleResize = () => {
        if (ref.current) {
          setMenuHeight(ref.current.clientHeight + 2)
        }
      }

      window.addEventListener('resize', handleResize)
    })

    const showMore = () => {
      toggleMenu(!open)
    }

    const { options } = useContext(WaxContext)

    const users = sharedUsers.map(([id, { user }]) => ({
      userId: user.id,
      displayName: user.displayName,
      currentUser: user.id === yjsCurrentUser.id,
    }))

    const { fullScreen } = options

    let fullScreenStyles = {}

    if (fullScreen) {
      fullScreenStyles = {
        backgroundColor: '#fff',
        left: '0',
        margin: '0',
        padding: '0',
        position: 'fixed',
        top: '0',
        width: '100%',
        zIndex: '1',
      }
    }

    return (
      <ThemeProvider theme={theme}>
        <Wrapper
          id="wax-container"
          menuHeight={menuHeight}
          style={fullScreenStyles}
        >
          <MenuWrapper>
            {main && (
              <MenuComponent fullScreen={fullScreen} open={open} ref={ref} />
            )}
            <ShowMore onClick={showMore} />
          </MenuWrapper>
          <CokoDocsEditorWrapper>
            {showFilemanager && (
              <FileManagerWrapper>{DocTreeManager}</FileManagerWrapper>
            )}
            <EditorArea>
              <Scrollbars>
                <WaxSurfaceScroll>
                  <div style={{ height: '100%' }}>
                    <EditorContainer>{editor}</EditorContainer>
                  </div>
                  <CommentsContainer>
                    <RightArea area="main" users={users} />
                  </CommentsContainer>
                </WaxSurfaceScroll>
              </Scrollbars>
            </EditorArea>
          </CokoDocsEditorWrapper>
          <WaxBottomRightInfo>
            <InfoContainer id="info-container">
              <BottomRightInfo />
            </InfoContainer>
          </WaxBottomRightInfo>
        </Wrapper>
      </ThemeProvider>
    )
  }

export default Layout
