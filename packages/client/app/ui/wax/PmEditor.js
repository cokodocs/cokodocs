/* eslint-disable react/prop-types */

import React, { useContext, useEffect, useState } from 'react'
import { useLazyQuery, gql } from '@apollo/client'
import styled from 'styled-components'
import PropTypes from 'prop-types'
import { yCursorPlugin, ySyncPlugin, yUndoPlugin } from 'y-prosemirror'
import { Wax } from 'wax-prosemirror-core'

import { useHistory } from 'react-router-dom'
import usePrintArea from './usePrintArea'
import config from './config/config'
import layout from './layout'
import YjsContext from '../../yjsProvider'
import { Result, Spin } from '../common'

const SpinnerWrapper = styled.div`
  display: ${props => (props.showSpinner ? 'block' : 'none')};
  left: ${props => (props.showFilemanager ? '59%' : '50%')};
  margin-left: -400px;
  margin-top: -25px;
  position: absolute;
  top: 50%;
  z-index: 999;
`

const GET_DOCUMENT = gql`
  query GetDocument($identifier: ID!) {
    getDocument(identifier: $identifier) {
      id
      identifier
      owner {
        id
      }
    }
  }
`

const renderImage = file => {
  const reader = new FileReader()

  return new Promise((resolve, reject) => {
    reader.onload = () => resolve(reader.result)
    reader.onerror = () => reject(reader.error)
    // Some extra delay to make the asynchronicity visible
    setTimeout(() => reader.readAsDataURL(file), 150)
  })
}

const PmEditor = ({ docIdentifier, docTreeManager, showFilemanager }) => {
  const history = useHistory()

  const { createYjsProvider, yjsProvider, ydoc, yjsCurrentUser } =
    useContext(YjsContext)

  const [reconfigureStateOptions, setReconfigureStateOptions] = useState({})
  const [readonly, setReadOnly] = useState(false)
  const [showSpinner, setShowSpinner] = useState(false)

  const { refElement } = usePrintArea({})

  const [getDocument] = useLazyQuery(GET_DOCUMENT, {
    variables: {
      identifier: docIdentifier,
    },
    onCompleted: ({ getDocument: doc }) => {
      setReadOnly(doc.owner ? !(doc.owner.id === yjsCurrentUser.id) : false)
    },
    fetchPolicy: 'no-cache',
  })

  useEffect(() => {
    if (docIdentifier) {
      yjsProvider?.disconnect()
      setShowSpinner(true)
      setTimeout(() => {
        createYjsProvider(docIdentifier)
        getDocument({ variables: { identifier } })
      }, 1000)

      setTimeout(() => {
        setShowSpinner(false)
      }, 2500)
    }
  }, [docIdentifier])

  useEffect(() => {
    if (yjsProvider) {
      const type = ydoc.getXmlFragment('prosemirror')

      setReconfigureStateOptions({
        docIdentifier: ydoc?.guid,
        plugins: [
          { ySyncPlugin: ySyncPlugin(type) },
          { yCursorPlugin: yCursorPlugin(yjsProvider.awareness) },
          { yUndoPlugin: yUndoPlugin() },
        ],
      })
    }
  }, [ydoc?.guid || ''])

  let identifier = docIdentifier

  if (!docIdentifier) {
    identifier = Array.from(Array(20), () =>
      Math.floor(Math.random() * 36).toString(36),
    ).join('')

    history.push(`/${identifier}`, { replace: true })
    return true
  }

  if (!yjsProvider || !ydoc) return null
  return (
    <>
      <Wax
        config={config(yjsProvider, ydoc)}
        fileUpload={file => renderImage(file)}
        layout={layout(docTreeManager, showFilemanager)}
        placeholder="Type Something ..."
        readonly={readonly}
        reconfigureState={reconfigureStateOptions}
        ref={refElement}
        scrollThreshold={50}
      />
      <SpinnerWrapper
        showFilemanager={showFilemanager}
        showSpinner={showSpinner}
      >
        <Result
          icon={<Spin size={18} spinning />}
          title="Loading your document"
        />
      </SpinnerWrapper>
    </>
  )
}

PmEditor.propTypes = {
  docIdentifier: PropTypes.string,
}

PmEditor.defaultProps = {
  docIdentifier: null,
}

export default PmEditor
